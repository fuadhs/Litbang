program Litbang;

uses
  System.StartUpCopy,
  FMX.Forms,
  MainUnit in 'Unit\MainUnit.pas' {Form1},
  uDM in 'Unit\uDM.pas' {DM: TDataModule},
  uPekerjaan in 'Unit\uPekerjaan.pas' {frmPekerjaan};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmPekerjaan, frmPekerjaan);
  Application.Run;
end.
