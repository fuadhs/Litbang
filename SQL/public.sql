/*
Navicat PGSQL Data Transfer

Source Server         : local
Source Server Version : 90623
Source Host           : localhost:5432
Source Database       : litbang
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90623
File Encoding         : 65001

Date: 2021-11-22 13:35:02
*/


-- ----------------------------
-- Sequence structure for failed_jobs_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."failed_jobs_id_seq";
CREATE SEQUENCE "public"."failed_jobs_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for m_afdeling_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_afdeling_id_seq";
CREATE SEQUENCE "public"."m_afdeling_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 83
 CACHE 1;
SELECT setval('"public"."m_afdeling_id_seq"', 83, true);

-- ----------------------------
-- Sequence structure for m_astan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_astan_id_seq";
CREATE SEQUENCE "public"."m_astan_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for m_blok_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_blok_id_seq";
CREATE SEQUENCE "public"."m_blok_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for m_harga_satuan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_harga_satuan_id_seq";
CREATE SEQUENCE "public"."m_harga_satuan_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."m_harga_satuan_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for m_kategori_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_kategori_id_seq";
CREATE SEQUENCE "public"."m_kategori_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."m_kategori_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for m_kebun_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_kebun_id_seq";
CREATE SEQUENCE "public"."m_kebun_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 16
 CACHE 1;
SELECT setval('"public"."m_kebun_id_seq"', 16, true);

-- ----------------------------
-- Sequence structure for m_mandor_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_mandor_id_seq";
CREATE SEQUENCE "public"."m_mandor_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for m_pekerjaan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_pekerjaan_id_seq";
CREATE SEQUENCE "public"."m_pekerjaan_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for m_petak_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_petak_id_seq";
CREATE SEQUENCE "public"."m_petak_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 50
 CACHE 1;
SELECT setval('"public"."m_petak_id_seq"', 50, true);

-- ----------------------------
-- Sequence structure for m_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_status_id_seq";
CREATE SEQUENCE "public"."m_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 6
 CACHE 1;
SELECT setval('"public"."m_status_id_seq"', 6, true);

-- ----------------------------
-- Sequence structure for m_tanam_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_tanam_id_seq";
CREATE SEQUENCE "public"."m_tanam_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for m_varietas_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_varietas_id_seq";
CREATE SEQUENCE "public"."m_varietas_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
CREATE SEQUENCE "public"."migrations_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 20
 CACHE 1;
SELECT setval('"public"."migrations_id_seq"', 20, true);

-- ----------------------------
-- Sequence structure for permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."permissions_id_seq";
CREATE SEQUENCE "public"."permissions_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 16
 CACHE 1;
SELECT setval('"public"."permissions_id_seq"', 16, true);

-- ----------------------------
-- Sequence structure for personal_access_tokens_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."personal_access_tokens_id_seq";
CREATE SEQUENCE "public"."personal_access_tokens_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for roles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."roles_id_seq";
CREATE SEQUENCE "public"."roles_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 5
 CACHE 1;
SELECT setval('"public"."roles_id_seq"', 5, true);

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS "public"."failed_jobs";
CREATE TABLE "public"."failed_jobs" (
"id" int8 DEFAULT nextval('failed_jobs_id_seq'::regclass) NOT NULL,
"connection" text COLLATE "default" NOT NULL,
"queue" text COLLATE "default" NOT NULL,
"payload" text COLLATE "default" NOT NULL,
"exception" text COLLATE "default" NOT NULL,
"failed_at" timestamp(0) DEFAULT now() NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for m_afdeling
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_afdeling";
CREATE TABLE "public"."m_afdeling" (
"id" int4 DEFAULT nextval('m_afdeling_id_seq'::regclass) NOT NULL,
"id_kebun" int4 NOT NULL,
"nama_afdeling" varchar(50) COLLATE "default" NOT NULL,
"luas" float8,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_afdeling
-- ----------------------------
INSERT INTO "public"."m_afdeling" VALUES ('1', '6', 'Timur', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('2', '1', 'Wangkal', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('3', '2', 'Sungailembu', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('4', '2', 'Pacauda', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('5', '2', 'Sumber Bopong', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('6', '3', 'Sumberjambe', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('7', '3', 'Sumber Waringin', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('8', '3', 'Sumber Gandeng', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('9', '3', 'Paal', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('10', '4', 'Kaliwadung', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('11', '4', 'Waringin', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('12', '4', 'Kalijambe', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('13', '4', 'Margosugih', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('14', '5', 'Darungan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('15', '5', 'Kempit', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('16', '5', 'Purwojoyo', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('17', '5', 'Kampung Lima', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('18', '6', 'Sumber Manggis', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('19', '6', 'Sumber Tempur', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('20', '6', 'Porolinggo', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('21', '6', 'Kalitelepak', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('22', '6', 'Polehan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('23', '7', 'Besaran', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('24', '7', 'Sumberurip', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('25', '7', 'Sidomukti', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('26', '7', 'Muktisari', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('27', '7', 'Sidodadi', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('28', '7', 'Pegundangan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('29', '7', 'Sekarbaru', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('30', '8', 'Besaran', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('31', '8', 'Rejosari', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('32', '8', 'Kaliputih', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('33', '8', 'Gentengan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('34', '8', 'Kampung Anyar', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('35', '8', 'Semampir', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('36', '8', 'Pagar Gunung', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('37', '9', 'Kalibaru Kidul', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('38', '9', 'Sumberbaru', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('39', '9', 'Kajar', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('40', '9', 'Kacangan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('41', '9', 'Jatirono Utara', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('42', '9', 'Sumber Salak', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('43', '9', 'Gunung Raung', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('44', '10', 'Sumber Jambe', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('45', '10', 'Gumitir', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('46', '11', 'Kalimayang', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('47', '11', 'Bajing Onjur', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('48', '11', 'Kali Bajing', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('49', '11', 'Wonojati', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('50', '11', 'Sumber Waringin', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('51', '12', 'Utara', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('52', '12', 'Selatan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('53', '12', 'Pondoksuto', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('54', '13', 'Blater', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('55', '13', 'Guci Putih', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('56', '13', 'Trate', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('57', '13', 'Banjar Agung', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('58', '14', 'Lengkong', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('59', '14', 'Mrawan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('60', '14', 'Gambiran', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('61', '14', 'Dampar', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('62', '14', 'Talang', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('63', '15', 'Curah Manis', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('64', '15', 'Sidomulyo', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('65', '16', 'Banjarsari', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('66', '16', 'Karangnangka', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('67', '16', 'Antokan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('68', '16', 'Gerengrejo', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('69', '16', 'Klatakan', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('70', '11', 'BajingOnjur', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('71', '9', 'GunungRaung', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('72', '9', 'JatironoUtara', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('73', '9', 'SumberSalak', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('74', '5', 'KampungLima', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('75', '6', 'SumberManggis', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('76', '6', 'SumberTempur', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('77', '8', 'KampungAnyar', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('78', '8', 'PagarGunung', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('79', '13', 'BanjarAgung', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('80', '13', 'GuciPutih', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('81', '3', 'SumberGandeng', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('82', '3', 'SumberWaringin', null, null, null);
INSERT INTO "public"."m_afdeling" VALUES ('83', '2', 'SumberBopong', null, null, null);

-- ----------------------------
-- Table structure for m_astan
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_astan";
CREATE TABLE "public"."m_astan" (
"id" int4 DEFAULT nextval('m_astan_id_seq'::regclass) NOT NULL,
"kd_astan" varchar(20) COLLATE "default" NOT NULL,
"nama_astan" varchar(40) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_astan
-- ----------------------------

-- ----------------------------
-- Table structure for m_blok
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_blok";
CREATE TABLE "public"."m_blok" (
"id" int8 DEFAULT nextval('m_blok_id_seq'::regclass) NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_blok
-- ----------------------------

-- ----------------------------
-- Table structure for m_harga_satuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_harga_satuan";
CREATE TABLE "public"."m_harga_satuan" (
"id" int4 DEFAULT nextval('m_harga_satuan_id_seq'::regclass) NOT NULL,
"id_pekerjaan" int4 NOT NULL,
"harga" float8 NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_harga_satuan
-- ----------------------------
INSERT INTO "public"."m_harga_satuan" VALUES ('1', '1', '8720', null, null);
INSERT INTO "public"."m_harga_satuan" VALUES ('2', '2', '5000', null, null);
INSERT INTO "public"."m_harga_satuan" VALUES ('3', '3', '8000', null, null);

-- ----------------------------
-- Table structure for m_kategori
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_kategori";
CREATE TABLE "public"."m_kategori" (
"id" int4 DEFAULT nextval('m_kategori_id_seq'::regclass) NOT NULL,
"kategori" char(5) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_kategori
-- ----------------------------
INSERT INTO "public"."m_kategori" VALUES ('1', 'TG   ', null, null);

-- ----------------------------
-- Table structure for m_kebun
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_kebun";
CREATE TABLE "public"."m_kebun" (
"id" int4 DEFAULT nextval('m_kebun_id_seq'::regclass) NOT NULL,
"kd_kebun" varchar(20) COLLATE "default" NOT NULL,
"nama_kebun" varchar(30) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_kebun
-- ----------------------------
INSERT INTO "public"."m_kebun" VALUES ('1', 'LK02', 'Kaliselogiri', null, null);
INSERT INTO "public"."m_kebun" VALUES ('2', 'LK03', 'Sungailembu', null, null);
INSERT INTO "public"."m_kebun" VALUES ('3', 'LK04', 'Sumber Jambe', null, null);
INSERT INTO "public"."m_kebun" VALUES ('4', 'LK05', 'Kalikempit', null, null);
INSERT INTO "public"."m_kebun" VALUES ('5', 'LK06', 'Kalisepanjang', null, null);
INSERT INTO "public"."m_kebun" VALUES ('6', 'LK07', 'Kalitelepak', null, null);
INSERT INTO "public"."m_kebun" VALUES ('7', 'LK08', 'Kalirejo', null, null);
INSERT INTO "public"."m_kebun" VALUES ('8', 'LK09', 'Kendenglembu', null, null);
INSERT INTO "public"."m_kebun" VALUES ('9', 'LK10', 'Jatirono', null, null);
INSERT INTO "public"."m_kebun" VALUES ('10', 'LK18', 'Sumber Tengah', null, null);
INSERT INTO "public"."m_kebun" VALUES ('11', 'LK19', 'Glantangan', null, null);
INSERT INTO "public"."m_kebun" VALUES ('12', 'LK20', 'Kalisanen', null, null);
INSERT INTO "public"."m_kebun" VALUES ('13', 'LK21', 'Kotta Blater', null, null);
INSERT INTO "public"."m_kebun" VALUES ('14', 'LK22', 'Mumbul', null, null);
INSERT INTO "public"."m_kebun" VALUES ('15', 'LK23', 'Renteng', null, null);
INSERT INTO "public"."m_kebun" VALUES ('16', '2414', 'Banjarsari', null, null);

-- ----------------------------
-- Table structure for m_mandor
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_mandor";
CREATE TABLE "public"."m_mandor" (
"id" int4 DEFAULT nextval('m_mandor_id_seq'::regclass) NOT NULL,
"kd_mandor" varchar(20) COLLATE "default" NOT NULL,
"nama_mandor" varchar(40) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_mandor
-- ----------------------------

-- ----------------------------
-- Table structure for m_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_pekerjaan";
CREATE TABLE "public"."m_pekerjaan" (
"id" int4 DEFAULT nextval('m_pekerjaan_id_seq'::regclass) NOT NULL,
"nama_pekerjaan" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_pekerjaan
-- ----------------------------
INSERT INTO "public"."m_pekerjaan" VALUES ('1', 'Permbersihan Lahan / bakar daduk', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('2', 'Pasang Anjir', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('3', 'Got Komplit', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('4', 'Bajak I', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('5', 'Bajak II', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('6', 'Kair', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('7', 'Tanam', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('8', 'Kepras', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('9', 'Sulam I', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('10', 'Sulam II', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('11', 'Putus Akar', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('12', 'Pupuk I (sufa)', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('13', 'Pupuk Extra', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('14', 'ZPT', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('15', 'Blotong', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('16', 'Siram ', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('17', 'Bumbun I', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('18', 'Herbisida I', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('19', 'Herbisida II', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('20', 'Got Keliling 2x', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('21', 'Got malang 2x', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('22', 'Rewos ', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('23', 'Klentek I', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('24', 'Klentek II', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('25', 'ikat Tebu', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('26', 'Umbal / Bandang', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('27', 'Songkol/campur pupuk', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('28', 'Bongkar/Stapel', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('29', 'Persiapan FA', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('30', 'Perbaikan saluran', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('31', 'Hama Kutu bulu putih', null, null);
INSERT INTO "public"."m_pekerjaan" VALUES ('32', 'Keamanan', null, null);

-- ----------------------------
-- Table structure for m_petak
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_petak";
CREATE TABLE "public"."m_petak" (
"id" int4 DEFAULT nextval('m_petak_id_seq'::regclass) NOT NULL,
"petak" varchar(5) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_petak
-- ----------------------------
INSERT INTO "public"."m_petak" VALUES ('1', '01', null, null);
INSERT INTO "public"."m_petak" VALUES ('2', '02', null, null);
INSERT INTO "public"."m_petak" VALUES ('3', '03', null, null);
INSERT INTO "public"."m_petak" VALUES ('4', '04', null, null);
INSERT INTO "public"."m_petak" VALUES ('5', '05', null, null);
INSERT INTO "public"."m_petak" VALUES ('6', '06', null, null);
INSERT INTO "public"."m_petak" VALUES ('7', '07', null, null);
INSERT INTO "public"."m_petak" VALUES ('8', '08', null, null);
INSERT INTO "public"."m_petak" VALUES ('9', '09', null, null);
INSERT INTO "public"."m_petak" VALUES ('10', '10', null, null);
INSERT INTO "public"."m_petak" VALUES ('11', '11', null, null);
INSERT INTO "public"."m_petak" VALUES ('12', '12', null, null);
INSERT INTO "public"."m_petak" VALUES ('13', '13', null, null);
INSERT INTO "public"."m_petak" VALUES ('14', '14', null, null);
INSERT INTO "public"."m_petak" VALUES ('15', '15', null, null);
INSERT INTO "public"."m_petak" VALUES ('16', '16', null, null);
INSERT INTO "public"."m_petak" VALUES ('17', '17', null, null);
INSERT INTO "public"."m_petak" VALUES ('18', '18', null, null);
INSERT INTO "public"."m_petak" VALUES ('19', '19', null, null);
INSERT INTO "public"."m_petak" VALUES ('20', '20', null, null);
INSERT INTO "public"."m_petak" VALUES ('21', '21', null, null);
INSERT INTO "public"."m_petak" VALUES ('22', '22', null, null);
INSERT INTO "public"."m_petak" VALUES ('23', '23', null, null);
INSERT INTO "public"."m_petak" VALUES ('24', '24', null, null);
INSERT INTO "public"."m_petak" VALUES ('25', '25', null, null);
INSERT INTO "public"."m_petak" VALUES ('26', '26', null, null);
INSERT INTO "public"."m_petak" VALUES ('27', '27', null, null);
INSERT INTO "public"."m_petak" VALUES ('28', '28', null, null);
INSERT INTO "public"."m_petak" VALUES ('29', '29', null, null);
INSERT INTO "public"."m_petak" VALUES ('30', '30', null, null);
INSERT INTO "public"."m_petak" VALUES ('31', '31', null, null);
INSERT INTO "public"."m_petak" VALUES ('32', '32', null, null);
INSERT INTO "public"."m_petak" VALUES ('33', '33', null, null);
INSERT INTO "public"."m_petak" VALUES ('34', '34', null, null);
INSERT INTO "public"."m_petak" VALUES ('35', '35', null, null);
INSERT INTO "public"."m_petak" VALUES ('36', '36', null, null);
INSERT INTO "public"."m_petak" VALUES ('37', '37', null, null);
INSERT INTO "public"."m_petak" VALUES ('38', '38', null, null);
INSERT INTO "public"."m_petak" VALUES ('39', '39', null, null);
INSERT INTO "public"."m_petak" VALUES ('40', '40', null, null);
INSERT INTO "public"."m_petak" VALUES ('41', '41', null, null);
INSERT INTO "public"."m_petak" VALUES ('42', '42', null, null);
INSERT INTO "public"."m_petak" VALUES ('43', '43', null, null);
INSERT INTO "public"."m_petak" VALUES ('44', '44', null, null);
INSERT INTO "public"."m_petak" VALUES ('45', '45', null, null);
INSERT INTO "public"."m_petak" VALUES ('46', '46', null, null);
INSERT INTO "public"."m_petak" VALUES ('47', '47', null, null);
INSERT INTO "public"."m_petak" VALUES ('48', '48', null, null);
INSERT INTO "public"."m_petak" VALUES ('49', '49', null, null);
INSERT INTO "public"."m_petak" VALUES ('50', '50', null, null);

-- ----------------------------
-- Table structure for m_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_status";
CREATE TABLE "public"."m_status" (
"id" int4 DEFAULT nextval('m_status_id_seq'::regclass) NOT NULL,
"status" char(5) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_status
-- ----------------------------
INSERT INTO "public"."m_status" VALUES ('1', 'KBD  ', null, null);
INSERT INTO "public"."m_status" VALUES ('2', 'PC   ', null, null);
INSERT INTO "public"."m_status" VALUES ('3', 'R1   ', null, null);
INSERT INTO "public"."m_status" VALUES ('4', 'R2   ', null, null);
INSERT INTO "public"."m_status" VALUES ('5', 'R3   ', null, null);
INSERT INTO "public"."m_status" VALUES ('6', 'R4   ', null, null);

-- ----------------------------
-- Table structure for m_tanam
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_tanam";
CREATE TABLE "public"."m_tanam" (
"id" int4 DEFAULT nextval('m_tanam_id_seq'::regclass) NOT NULL,
"masa_tanam" varchar(10) COLLATE "default" NOT NULL,
"tgl_mulai" date NOT NULL,
"tgl_akhir" date NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_tanam
-- ----------------------------

-- ----------------------------
-- Table structure for m_varietas
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_varietas";
CREATE TABLE "public"."m_varietas" (
"id" int4 DEFAULT nextval('m_varietas_id_seq'::regclass) NOT NULL,
"nama_varietas" varchar(20) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of m_varietas
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."migrations";
CREATE TABLE "public"."migrations" (
"id" int4 DEFAULT nextval('migrations_id_seq'::regclass) NOT NULL,
"migration" varchar(255) COLLATE "default" NOT NULL,
"batch" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO "public"."migrations" VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO "public"."migrations" VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO "public"."migrations" VALUES ('3', '2019_03_16_102314_add_simple_role_to_user_table', '1');
INSERT INTO "public"."migrations" VALUES ('4', '2019_04_20_125200_create_permission_tables', '1');
INSERT INTO "public"."migrations" VALUES ('5', '2019_04_20_130706_setup_role_permissions', '1');
INSERT INTO "public"."migrations" VALUES ('6', '2019_08_19_000000_create_failed_jobs_table', '1');
INSERT INTO "public"."migrations" VALUES ('7', '2019_12_14_000001_create_personal_access_tokens_table', '1');
INSERT INTO "public"."migrations" VALUES ('8', '2020_03_25_170854_remove_passport', '1');
INSERT INTO "public"."migrations" VALUES ('9', '2021_11_22_030207_create_table_m_kebun', '1');
INSERT INTO "public"."migrations" VALUES ('10', '2021_11_22_030236_create_table_m_afdeling', '1');
INSERT INTO "public"."migrations" VALUES ('11', '2021_11_22_030302_create_table_m_varietas', '1');
INSERT INTO "public"."migrations" VALUES ('12', '2021_11_22_030353_create_table_m_tanam', '1');
INSERT INTO "public"."migrations" VALUES ('13', '2021_11_22_030453_create_table_m_mandor', '1');
INSERT INTO "public"."migrations" VALUES ('14', '2021_11_22_030510_create_table_m_perkerjaan', '1');
INSERT INTO "public"."migrations" VALUES ('15', '2021_11_22_030511_create_table_m_astan', '1');
INSERT INTO "public"."migrations" VALUES ('16', '2021_11_22_032934_create_table_m_blok', '2');
INSERT INTO "public"."migrations" VALUES ('17', '2021_11_22_052849_create_table_m_harga_satuan', '3');
INSERT INTO "public"."migrations" VALUES ('18', '2021_11_22_053459_create_table_m_petak', '4');
INSERT INTO "public"."migrations" VALUES ('19', '2021_11_22_053546_create_table_m_kategori', '4');
INSERT INTO "public"."migrations" VALUES ('20', '2021_11_22_053603_create_table_m_status', '4');

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."model_has_permissions";
CREATE TABLE "public"."model_has_permissions" (
"permission_id" int4 NOT NULL,
"model_type" varchar(255) COLLATE "default" NOT NULL,
"model_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of model_has_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."model_has_roles";
CREATE TABLE "public"."model_has_roles" (
"role_id" int4 NOT NULL,
"model_type" varchar(255) COLLATE "default" NOT NULL,
"model_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS "public"."password_resets";
CREATE TABLE "public"."password_resets" (
"email" varchar(255) COLLATE "default" NOT NULL,
"token" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."permissions";
CREATE TABLE "public"."permissions" (
"id" int4 DEFAULT nextval('permissions_id_seq'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"guard_name" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO "public"."permissions" VALUES ('1', 'view menu element ui', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('2', 'view menu permission', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('3', 'view menu components', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('4', 'view menu charts', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('5', 'view menu nested routes', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('6', 'view menu table', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('7', 'view menu administrator', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('8', 'view menu theme', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('9', 'view menu clipboard', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('10', 'view menu excel', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('11', 'view menu zip', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('12', 'view menu pdf', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('13', 'view menu i18n', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('14', 'manage user', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('15', 'manage article', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."permissions" VALUES ('16', 'manage permission', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS "public"."personal_access_tokens";
CREATE TABLE "public"."personal_access_tokens" (
"id" int8 DEFAULT nextval('personal_access_tokens_id_seq'::regclass) NOT NULL,
"tokenable_type" varchar(255) COLLATE "default" NOT NULL,
"tokenable_id" int8 NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"token" varchar(64) COLLATE "default" NOT NULL,
"abilities" text COLLATE "default",
"last_used_at" timestamp(0),
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."role_has_permissions";
CREATE TABLE "public"."role_has_permissions" (
"permission_id" int4 NOT NULL,
"role_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO "public"."role_has_permissions" VALUES ('1', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('1', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('1', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('1', '4');
INSERT INTO "public"."role_has_permissions" VALUES ('1', '5');
INSERT INTO "public"."role_has_permissions" VALUES ('2', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('2', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('2', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('2', '4');
INSERT INTO "public"."role_has_permissions" VALUES ('2', '5');
INSERT INTO "public"."role_has_permissions" VALUES ('3', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('3', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('3', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('4', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('4', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('4', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('5', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('5', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('5', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('6', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('6', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('6', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('7', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('7', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('7', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('8', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('8', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('8', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('9', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('9', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('9', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('10', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('10', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('10', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('11', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('11', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('11', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('12', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('12', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('12', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('13', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('13', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('13', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('14', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('14', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('15', '1');
INSERT INTO "public"."role_has_permissions" VALUES ('15', '2');
INSERT INTO "public"."role_has_permissions" VALUES ('15', '3');
INSERT INTO "public"."role_has_permissions" VALUES ('16', '1');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."roles";
CREATE TABLE "public"."roles" (
"id" int4 DEFAULT nextval('roles_id_seq'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"guard_name" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO "public"."roles" VALUES ('1', 'admin', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."roles" VALUES ('2', 'manager', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."roles" VALUES ('3', 'editor', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."roles" VALUES ('4', 'user', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');
INSERT INTO "public"."roles" VALUES ('5', 'visitor', 'api', '2021-11-22 03:28:26', '2021-11-22 03:28:26');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
"id" int8 DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"email" varchar(255) COLLATE "default" NOT NULL,
"email_verified_at" timestamp(0),
"password" varchar(255) COLLATE "default" NOT NULL,
"remember_token" varchar(100) COLLATE "default",
"created_at" timestamp(0),
"updated_at" timestamp(0)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."failed_jobs_id_seq" OWNED BY "failed_jobs"."id";
ALTER SEQUENCE "public"."m_afdeling_id_seq" OWNED BY "m_afdeling"."id";
ALTER SEQUENCE "public"."m_astan_id_seq" OWNED BY "m_astan"."id";
ALTER SEQUENCE "public"."m_blok_id_seq" OWNED BY "m_blok"."id";
ALTER SEQUENCE "public"."m_harga_satuan_id_seq" OWNED BY "m_harga_satuan"."id";
ALTER SEQUENCE "public"."m_kategori_id_seq" OWNED BY "m_kategori"."id";
ALTER SEQUENCE "public"."m_kebun_id_seq" OWNED BY "m_kebun"."id";
ALTER SEQUENCE "public"."m_mandor_id_seq" OWNED BY "m_mandor"."id";
ALTER SEQUENCE "public"."m_pekerjaan_id_seq" OWNED BY "m_pekerjaan"."id";
ALTER SEQUENCE "public"."m_petak_id_seq" OWNED BY "m_petak"."id";
ALTER SEQUENCE "public"."m_status_id_seq" OWNED BY "m_status"."id";
ALTER SEQUENCE "public"."m_tanam_id_seq" OWNED BY "m_tanam"."id";
ALTER SEQUENCE "public"."m_varietas_id_seq" OWNED BY "m_varietas"."id";
ALTER SEQUENCE "public"."migrations_id_seq" OWNED BY "migrations"."id";
ALTER SEQUENCE "public"."permissions_id_seq" OWNED BY "permissions"."id";
ALTER SEQUENCE "public"."personal_access_tokens_id_seq" OWNED BY "personal_access_tokens"."id";
ALTER SEQUENCE "public"."roles_id_seq" OWNED BY "roles"."id";
ALTER SEQUENCE "public"."users_id_seq" OWNED BY "users"."id";

-- ----------------------------
-- Primary Key structure for table failed_jobs
-- ----------------------------
ALTER TABLE "public"."failed_jobs" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_afdeling
-- ----------------------------
ALTER TABLE "public"."m_afdeling" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_astan
-- ----------------------------
ALTER TABLE "public"."m_astan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_blok
-- ----------------------------
ALTER TABLE "public"."m_blok" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_harga_satuan
-- ----------------------------
ALTER TABLE "public"."m_harga_satuan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_kategori
-- ----------------------------
ALTER TABLE "public"."m_kategori" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_kebun
-- ----------------------------
ALTER TABLE "public"."m_kebun" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_mandor
-- ----------------------------
ALTER TABLE "public"."m_mandor" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_pekerjaan
-- ----------------------------
ALTER TABLE "public"."m_pekerjaan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_petak
-- ----------------------------
ALTER TABLE "public"."m_petak" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_status
-- ----------------------------
ALTER TABLE "public"."m_status" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_tanam
-- ----------------------------
ALTER TABLE "public"."m_tanam" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_varietas
-- ----------------------------
ALTER TABLE "public"."m_varietas" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "public"."migrations" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table model_has_permissions
-- ----------------------------
CREATE INDEX "model_has_permissions_model_id_model_type_index" ON "public"."model_has_permissions" USING btree ("model_id", "model_type");

-- ----------------------------
-- Primary Key structure for table model_has_permissions
-- ----------------------------
ALTER TABLE "public"."model_has_permissions" ADD PRIMARY KEY ("permission_id", "model_id", "model_type");

-- ----------------------------
-- Indexes structure for table model_has_roles
-- ----------------------------
CREATE INDEX "model_has_roles_model_id_model_type_index" ON "public"."model_has_roles" USING btree ("model_id", "model_type");

-- ----------------------------
-- Primary Key structure for table model_has_roles
-- ----------------------------
ALTER TABLE "public"."model_has_roles" ADD PRIMARY KEY ("role_id", "model_id", "model_type");

-- ----------------------------
-- Indexes structure for table password_resets
-- ----------------------------
CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree ("email");

-- ----------------------------
-- Primary Key structure for table permissions
-- ----------------------------
ALTER TABLE "public"."permissions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table personal_access_tokens
-- ----------------------------
CREATE INDEX "personal_access_tokens_tokenable_type_tokenable_id_index" ON "public"."personal_access_tokens" USING btree ("tokenable_type", "tokenable_id");

-- ----------------------------
-- Uniques structure for table personal_access_tokens
-- ----------------------------
ALTER TABLE "public"."personal_access_tokens" ADD UNIQUE ("token");

-- ----------------------------
-- Primary Key structure for table personal_access_tokens
-- ----------------------------
ALTER TABLE "public"."personal_access_tokens" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table role_has_permissions
-- ----------------------------
ALTER TABLE "public"."role_has_permissions" ADD PRIMARY KEY ("permission_id", "role_id");

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "public"."roles" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."model_has_permissions"
-- ----------------------------
ALTER TABLE "public"."model_has_permissions" ADD FOREIGN KEY ("permission_id") REFERENCES "public"."permissions" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."model_has_roles"
-- ----------------------------
ALTER TABLE "public"."model_has_roles" ADD FOREIGN KEY ("role_id") REFERENCES "public"."roles" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."role_has_permissions"
-- ----------------------------
ALTER TABLE "public"."role_has_permissions" ADD FOREIGN KEY ("role_id") REFERENCES "public"."roles" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "public"."role_has_permissions" ADD FOREIGN KEY ("permission_id") REFERENCES "public"."permissions" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
