unit uDM;

interface

uses
  System.SysUtils, System.Classes, UniProvider, PostgreSQLUniProvider, Data.DB,
  MemDS, DBAccess, Uni;

type
  TDM = class(TDataModule)
    con1: TUniConnection;
    Q1: TUniQuery;
    pg1: TPostgreSQLUniProvider;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

end.
