object DM: TDM
  OldCreateOrder = False
  Height = 340
  Width = 568
  object con1: TUniConnection
    ProviderName = 'postgreSQL'
    Port = 5432
    Database = 'litbang'
    Username = 'postgres'
    Server = 'localhost'
    Connected = True
    LoginPrompt = False
    Left = 272
    Top = 152
    EncryptedPassword = '9EFF9BFF92FF96FF91FFCEFFC6FFC6FF'
  end
  object Q1: TUniQuery
    Connection = con1
    SQL.Strings = (
      
        'SELECT row_number() over (ORDER BY a.id) as nomor,a.id,a.tgl,b.n' +
        'ama_kebun,c.nama_afdeling,d.nama_mandor,a.petak,a.kategori,a.lua' +
        's,a.masa_tanam,a.varietas,g.nama_pekerjaan,a.ohk,a.status,e.kd_b' +
        'lok  from t_pekerjaan a'
      'LEFT JOIN m_kebun b on b.id=a.id_kebun'
      'LEFT JOIN m_afdeling c on c.id=a.id_afdeling'
      'LEFT JOIN m_mandor d on d.id=a.id_mandor'
      'LEFT JOIN m_blok e on e.id=a.id_blok'
      'LEFT JOIN m_astan f on f.id=a.id_astan'
      'LEFT JOIN m_pekerjaan g on g.id=a.id_pekerjaan')
    Active = True
    Left = 320
    Top = 120
  end
  object pg1: TPostgreSQLUniProvider
    Left = 336
    Top = 168
  end
end
