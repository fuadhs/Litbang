unit uPekerjaan;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMXTee.Control,
  FMXTee.Grid, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit,
  System.ImageList, FMX.ImgList,
   Tee.Grid.Columns, Data.Bind.EngExt, Fmx.Bind.DBEngExt, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.DBScope, Data.DB, MemDS, DBAccess, Uni, FMX.DateTimeCtrls,
  FMX.ListBox, UI.Base, UI.Edit, DAScript, UniScript, FMX.TabControl,
  FMX.ComboEdit, uSkinFireMonkeyComboBox, FMX.EditBox, FMX.NumberBox,
  FMX.SpinBox,Tee.Grid, Tee.GridData,Tee.Grid.Selection, FMX.Layouts;

type
  TfrmPekerjaan = class(TForm)
    il1: TImageList;
    bndngslst1: TBindingsList;
    unscrpt1: TUniScript;
    tbc1: TTabControl;
    tbtm1: TTabItem;
    tbtm2: TTabItem;
    cbb1: TComboBox;
    CornerButton1: TCornerButton;
    CornerButton2: TCornerButton;
    CornerButton3: TCornerButton;
    dtdt1: TDateEdit;
    dtdt2: TDateEdit;
    edtCari: TEditView;
    lbl1: TLabel;
    lbl2: TLabel;
    tgrd1: TTeeGrid;
    QKebun: TUniQuery;
    QTemp: TUniQuery;
    QAfdeling: TUniQuery;
    QBlok: TUniQuery;
    QMandor: TUniQuery;
    pnl1: TPanel;
    cbMandor: TComboEdit;
    cbBlok: TComboEdit;
    cbAfdeling: TComboEdit;
    cbKebun: TComboEdit;
    sty1: TStyleBook;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    cbMT: TComboEdit;
    lbl7: TLabel;
    QMT: TUniQuery;
    QStatus: TUniQuery;
    cbStatus: TComboEdit;
    lbl8: TLabel;
    cbVarietas: TComboEdit;
    lbl9: TLabel;
    QVarietas: TUniQuery;
    edLuas: TNumberBox;
    edOhk: TNumberBox;
    lbl11: TLabel;
    lbl10: TLabel;
    pnl3: TPanel;
    tgrd3: TTeeGrid;
    QPekerjaan: TUniQuery;
    QTempKerja: TUniQuery;
    CornerButton4: TCornerButton;
    CornerButton5: TCornerButton;
    btnSave: TCornerButton;
    edID: TNumberBox;
    bndsrcdb1: TBindSourceDB;
    QGet: TUniQuery;
    lnkflcntrltfld1: TLinkFillControlToField;
    lnkflcntrltfld2: TLinkFillControlToField;
    lKebun: TLabel;
    lAfdeling: TLabel;
    lblok: TLabel;
    lmandor: TLabel;
    lvarietas: TLabel;
    QKebun1: TUniQuery;
    QAfdeling1: TUniQuery;
    QBlok1: TUniQuery;
    QMandor1: TUniQuery;
    QAstan1: TUniQuery;
    QTampil: TUniQuery;
    QCari: TUniQuery;
    QPetak: TUniQuery;
    QAstan: TUniQuery;
    dt_tgl: TDateEdit;
    lbl13: TLabel;
    cbAstan: TComboEdit;
    lbl14: TLabel;
    lAstan: TLabel;
    cbPetak: TComboEdit;
    lbl15: TLabel;
    lKerja: TLabel;
    lbl16: TLabel;
    lnkprprtytfldText: TLinkPropertyToField;
    bndsrcdb2: TBindSourceDB;
    lnkflcntrltfld3: TLinkFillControlToField;
    QTampil2: TUniQuery;
    QTampil3: TUniQuery;
    pnl4: TPanel;
    tgrd5: TTeeGrid;
    tgrd4: TTeeGrid;
    tgrd2: TTeeGrid;
    cbPekerjaan: TComboEdit;
    lbl17: TLabel;

    procedure CornerButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dtdt1Change(Sender: TObject);
    procedure edtCariChange(Sender: TObject);
    procedure edt1Change(Sender: TObject);
    procedure CornerButton1Click(Sender: TObject);
    procedure edOhkKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbKebunKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbAfdelingKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbBlokKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbMandorKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbVarietasKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbStatusKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure edLuasKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbMTKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure btnSaveClick(Sender: TObject);
    procedure cbKebunChange(Sender: TObject);
    procedure cbAfdelingChange(Sender: TObject);
    procedure cbBlokChange(Sender: TObject);
    procedure cbMandorChange(Sender: TObject);
    procedure cbAstanChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dt_tglKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbAstanKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbPetakKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure edIDChange(Sender: TObject);
    procedure edIDKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure cbPekerjaanChange(Sender: TObject);
    procedure cbPekerjaanKeyUp(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
  private
    { Private declarations }
    procedure getdata;
    procedure Savedata;
  public
    { Public declarations }
  end;

var
  frmPekerjaan: TfrmPekerjaan;
   idKebun,idAfdeling,idBlok,idMandor,idAstan,idVarietas,idStatus,idkerjaan:Integer;

implementation

uses
  uDM, MainUnit;

{$R *.fmx}

procedure TfrmPekerjaan.getdata;
//var
// Qkebun,QAfdeling,QBlok,QMandor :TUniQuery;
begin
//  Qkebun.Connection:=DM.con1;
//  QAfdeling.Connection:=DM.con1;
//  Qblok.Connection:=DM.con1;
//  QMandor.Connection:=DM.con1;
  with Qkebun do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_kebun');
    Open;
    while not Qkebun.Eof do
    begin
    cbKebun.AutoCapture:= True;
    cbKebun.Items.Add(QKebun.FieldByName('nama_kebun').AsString);
//combobox1.Items.Add(ADOquery1.FieldByName(�Nama_Barang�).AsString);
//edt1.AutoCapture:= True;
//    edt1.Items.Add(Qkebun.FieldByName('nama_kebun').AsString);
    Qkebun.Next;
    end;
//  end;
  with QAfdeling do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_afdeling');
    Open;
    while not QAfdeling.Eof do
    begin
    cbAfdeling.Items.Add(QAfdeling.FieldByName('nama_afdeling').AsString);
    QAfdeling.Next;
    end;
  end;
  with QBlok do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_blok');
    Open;
    while not QBlok.Eof do
    begin
    cbBlok.Items.Add(QBlok.FieldByName('kd_blok').AsString);
    QBlok.Next;
    end;
  end;
  with QAstan do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select * from m_astan');
    Open;
    while not QAstan.Eof do
    begin
    cbAstan.Items.Add(QAstan.FieldByName('nama_astan').AsString);
    QAstan.Next;
    end;
  end;
  with QMandor do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_mandor');
    Open;
    while not QMandor.Eof do
    begin
    cbMandor.Items.Add(QMandor.FieldByName('nama_mandor').AsString);
    QMandor.Next;
    end;
  end;
  with QMT do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_tanam');
    Open;
    while not QMT.Eof do
    begin
    cbMT.Items.Add(QMT.FieldByName('masa_tanam').AsString);
    QMT.Next;
    end;
  end;
  with QPetak do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_petak');
    Open;
    while not QPetak.Eof do
    begin
    cbPetak.Items.Add(QPetak.FieldByName('petak').AsString);
    QPetak.Next;
    end;
  end;
  with QStatus do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_status');
    Open;
    while not QStatus.Eof do
    begin
    cbStatus.Items.Add(QStatus.FieldByName('status').AsString);
    QStatus.Next;
    end;
  end;
  with QVarietas do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_varietas');
    Open;
    while not QVarietas.Eof do
    begin
    cbVarietas.Items.Add(QVarietas.FieldByName('nama_varietas').AsString);
    QVarietas.Next;
    end;
  end;
  end;
  with QPekerjaan do
  begin
    Close;
    SQL.Clear;
    sql.Add('select * from m_pekerjaan');
    Open;
    while not QPekerjaan.Eof do
    begin
    cbPekerjaan.Items.Add(QPekerjaan.FieldByName('nama_pekerjaan').AsString);
    QPekerjaan.Next;
    end;
  end;
end;

procedure TfrmPekerjaan.btnSaveClick(Sender: TObject);
begin
ShowMessage('ok');
end;

procedure TfrmPekerjaan.cbAfdelingKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if (cbAfdeling.Text<>'') and (Key=vkReturn) then
begin
  cbBlok.SetFocus;
end;
end;

procedure TfrmPekerjaan.cbBlokKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbBlok.Text<>'') and (Key=vkReturn) then
begin
  cbMandor.SetFocus;
end;
end;

procedure TfrmPekerjaan.cbKebunKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbKebun.Text<>'') and (Key=vkReturn) then
begin
  cbAfdeling.SetFocus;
end;
end;

procedure TfrmPekerjaan.cbKebunChange(Sender: TObject);
begin
 with QKebun1 do
begin
  Close;
  sql.Clear;
  SQL.Add('select id from m_kebun where nama_kebun='+#39+cbKebun.Text+#39+'');
  Open;
  lKebun.Text:=QKebun1.FieldByName('id').AsString;
  idKebun:= QKebun1.FieldByName('id').AsInteger;
end;
end;

procedure TfrmPekerjaan.cbAfdelingChange(Sender: TObject);
begin
with QAfdeling1 do
begin
  Close;
  SQL.Clear;
  SQL.Add('select id from m_afdeling where nama_afdeling='+#39+cbAfdeling.Text+#39+'');
  Open;
  lAfdeling.Text:= QAfdeling1.FieldByName('id').AsString;
  idAfdeling    := QAfdeling1.FieldByName('id').AsInteger;
end;
end;


procedure TfrmPekerjaan.cbBlokChange(Sender: TObject);
begin
  with QBlok1 do
begin
  Close;
  SQL.Clear;
  SQL.Add('select id from m_blok where kd_blok='+#39+cbBlok.Text+#39+'');
  Open;
  lblok.Text:= QBlok1.FieldByName('id').AsString;
  idBlok    := QBlok1.FieldByName('id').AsInteger;
end;
end;

procedure TfrmPekerjaan.cbMandorChange(Sender: TObject);
begin
  with QMandor1 do
begin
  Close;
  SQL.Clear;
  SQL.Add('select id from m_mandor where nama_mandor='+#39+cbMandor.Text+#39+'');
  Open;
  lmandor.Text := QMandor1.FieldByName('id').AsString;
  idMandor     := QMandor1.FieldByName('id').AsInteger;
end;
end;
procedure TfrmPekerjaan.cbAstanChange(Sender: TObject);
begin
  with QAstan1 do
begin
  Close;
  SQL.Clear;
  SQL.Add('select id from m_astan where nama_astan='+#39+cbAstan.Text+#39+'');
  Open;
  lAstan.Text := QAstan1.FieldByName('id').AsString;
  idAstan     := QAstan1.FieldByName('id').AsInteger;
end;
end;

procedure TfrmPekerjaan.cbAstanKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbAstan.Text<>'')and(Key=vkReturn) then
begin
  cbKebun.SetFocus;
end;
end;

procedure TfrmPekerjaan.cbMandorKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbMandor.Text<>'') and (Key=vkReturn) then
begin
  cbPetak.SetFocus;
end;
end;


procedure TfrmPekerjaan.cbMTKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbMT.Text<>'') and (Key=vkReturn) then
begin
  cbStatus.SetFocus;
end;
end;


procedure TfrmPekerjaan.cbPekerjaanChange(Sender: TObject);
begin
  with qcari do
  begin
  close;
  sql.Clear;
  SQL.Add('select * from m_pekerjaan where nama_pekerjaan='+#39+cbPekerjaan.Text+#39+'');
  Open;
  edID.Text:= qcari.FieldByName('id').AsString;
  end;
end;

procedure TfrmPekerjaan.cbPekerjaanKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbPekerjaan.Text<>'') and (Key=vkReturn) then
begin
  edOhk.SetFocus;
end;

end;

procedure TfrmPekerjaan.cbPetakKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbPetak.Text<>'') and (Key=vkReturn) then
begin
  cbVarietas.SetFocus;
end;
end;

procedure TfrmPekerjaan.cbStatusKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbStatus.Text<>'') and (Key=vkReturn) then
begin
  edLuas.SetFocus;
end;
end;


procedure TfrmPekerjaan.cbVarietasKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (cbVarietas.Text<>'') and (Key=vkReturn) then
begin
  cbMT.SetFocus;
end;
end;

procedure TfrmPekerjaan.CornerButton1Click(Sender: TObject);
begin
unscrpt1.Execute;
tbc1.TabIndex:=1;
end;

procedure TfrmPekerjaan.CornerButton3Click(Sender: TObject);
begin
 close;
end;

procedure TfrmPekerjaan.dtdt1Change(Sender: TObject);
begin
with DM.Q1 do
begin
 close;
 SQL.Clear;
 SQL.Add('select * from vw_pekerjaan where tgl BETWEEN '#39+FormatDateTime('yyyy-mm-dd',dtdt1.Date)+#39' AND '#39+FormatDateTime('yyyy-mm-dd',dtdt2.Date)+#39+'');
 ExecSQL;
end;
end;

procedure TfrmPekerjaan.dt_tglKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if Key=vkReturn then
begin
  cbAstan.SetFocus;
end;
end;

procedure TfrmPekerjaan.edIDChange(Sender: TObject);
begin
 with QCari do
 begin
   Close;
   SQL.Clear;
   SQL.Add('select * from m_pekerjaan where id='+#39+edID.Text+#39+'');
   Open;
   cbPekerjaan.Text:=QCari.FieldByName('nama_pekerjaan').AsString;
 end;
end;

procedure TfrmPekerjaan.edIDKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
 if Key=vkReturn then
 begin
   edOhk.SetFocus;
 end;
end;

procedure TfrmPekerjaan.edLuasKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if (edLuas.Value<>0) and (Key=vkReturn) then
begin
  cbPekerjaan.SetFocus;
end;
end;

procedure TfrmPekerjaan.edOhkKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
if Key=vkReturn then
begin
//btnSave.SetFocus;
Savedata;
end;
end;
procedure TfrmPekerjaan.Savedata;
//var
//xKebun,xAfdeling,xBlok,xMandor,xVarietas,xStatus,xkerjaan:string;
//idKebun,idAfdeling,idBlok,idMandor,idVarietas,idStatus,idkerjaan:Integer;
begin
//with
//with QKebun do
//begin
//  Close;
//  sql.Clear;
//  SQL.Add('select id from m_kebun where nama_kebun='+#39+cbKebun.Text+#39+'');
//  Open;
//  lKebun.Text:=QKebun.FieldByName('id').AsString;
//end;
  with QTemp do
  begin
    Close;
    sql.Clear;
    SQL.Add('insert into t_pekerjaan_tmp (id_kebun,id_afdeling,id_mandor,id_blok,tgl,'+
    'petak,kategori,status,varietas,masa_tanam,luas,ohk,id_astan,id_pekerjaan,'+
    'created_at,updated_at)values(:id_kebun,:id_afdeling,:id_mandor,:id_blok,:tgl,'+
    ':petak,:kategori,:status,:varietas,:masa_tanam,:luas,:ohk,:id_astan,:id_pekerjaan,:created_at,:updated_at)');
    ParamByName('id_kebun').AsInteger     := StrToInt(lKebun.Text);
    ParamByName('id_afdeling').AsInteger  := StrToInt(lAfdeling.Text);
    ParamByName('id_mandor').AsInteger    := StrToInt(lmandor.Text);
//    ParamByName('id_blok').AsInteger      := StrToInt(lblok.Text);
    ParamByName('tgl').AsDate              := dt_tgl.Date;
    ParamByName('petak').AsString         := cbPetak.Text;
//    ParamByName('kategori').AsString    := cbka.Text;
    ParamByName('status').AsString        := cbStatus.Text;
    ParamByName('varietas').AsString      := cbVarietas.Text;
    ParamByName('masa_tanam').AsString    := cbMT.Text;
    ParamByName('luas').AsFloat           := StrToFloat(edLuas.Text);
    ParamByName('ohk').AsInteger          := StrToInt(edOhk.Text);
    ParamByName('id_pekerjaan').AsInteger := StrToInt(cbPetak.Text);
    ParamByName('created_at').AsDate      := dt_tgl.Date;
//    ParamByName('updated_at').AsDate      :=
    ExecSQL;
    QTempKerja.Refresh;

  end;
end;

procedure TfrmPekerjaan.edt1Change(Sender: TObject);
begin
 case cbb1.ItemIndex of
  0: begin
      ShowMessage('Pilih kategori pencarian !!');
  end;
  1:begin
   with DM.Q1 do
    begin
     close;
     SQL.Clear;
     SQL.Add('select * from vw_pekerjaan where nama_kebun ilike '+#39+edtCari.Text+'%'+#39+'');
     ExecSQL;
     dm.Q1.Refresh;
    end;
  end;
  2:begin
   with DM.Q1 do
    begin
     close;
     SQL.Clear;
     SQL.Add('select * from vw_pekerjaan where nama_afdeling ilike '+#39+edtCari.Text+'%'+#39+'');
     ExecSQL;
     dm.Q1.Refresh;
    end;
  end;
  3:begin
   with DM.Q1 do
    begin
     close;
     SQL.Clear;
     SQL.Add('select * from vw_pekerjaan where kd_blok ilike '+#39+edtCari.Text+'%'+#39+'');
     ExecSQL;
     dm.Q1.Refresh;
    end;
  end;
  4:begin
   with DM.Q1 do
    begin
     close;
     SQL.Clear;
     SQL.Add('select * from vw_pekerjaan where nama_mandor ilike '+#39+edtCari.Text+'%'+#39+'');
     ExecSQL;
     dm.Q1.Refresh;
    end;
  end;
  5:begin
   with DM.Q1 do
    begin
     close;
     SQL.Clear;
     SQL.Add('select * from vw_pekerjaan where nama_pekerjaan ilike '+#39+edtCari.Text+'%'+#39+'');
     ExecSQL;
     dm.Q1.Refresh;
    end;
  end;
end;
end;

procedure TfrmPekerjaan.edtCariChange(Sender: TObject);
begin
case cbb1.ItemIndex of
  0: begin
      ShowMessage('Pilih kategori pencarian !!');
  end;
  1:begin
   with DM.Q1 do
    begin
     close;
     SQL.Clear;
     SQL.Add('select * from vw_pekerjaan where nama_kebun ilike '+#39+edtCari.Text+'%'+#39+'');
     ExecSQL;
     dm.Q1.Refresh;
    end;
  end;
end;
end;

procedure TfrmPekerjaan.FormCreate(Sender: TObject);
begin
getdata;
  // Use random sample data
//  TeeGrid1.Data:=SampleData;
//
//  // Set some column Formatting strings
//  TeeGrid1.Columns['Height'].DataFormat.Float:='0.##';
//  TeeGrid1.Columns['BirthDate'].DataFormat.DateTime:='mm-dd-yyyy';
//
//  // Do not show cell editor with all text selected by default
//  TeeGrid1.Editing.Text.Selected:=False;

//  SetupCustomEditors;
//SQL
//CREATE TABLE IF NOT EXISTS t_pekerjaan_tmp (
//   id serial,
//"id_kebun" int4 NOT NULL,
//"id_afdeling" int4 NOT NULL,
//"id_mandor" int4 NOT NULL,
//"id_blok" int4 NOT NULL,
//"tgl" date NOT NULL,
//"petak" varchar(10) COLLATE "default" NOT NULL,
//"kategori" varchar(10) COLLATE "default" NOT NULL,
//"status" varchar(10) COLLATE "default" NOT NULL,
//"varietas" varchar(10) COLLATE "default" NOT NULL,
//"areal" float8 NOT NULL,
//"masa_tanam" varchar(255) COLLATE "default" NOT NULL,
//"luas" float8 NOT NULL,
//"ohk" float8 NOT NULL,
//"id_astan" int4,
//"id_pekerjaan" int4,
//"created_at" timestamp(0),
//"updated_at" timestamp(0)
//);
//with QTemp do
//begin
//  Close;
//  SQL.Clear;
//  SQL.Add('DROP TABLE if EXISTS t_pekerjaan_tmp ');
//  ExecSQL;
//end;
with DM.Q1 do
begin
 close;
 SQL.Clear;
 SQL.Add('select * from vw_pekerjaan');// where tgl BETWEEN '#39+FormatDateTime('yyyy-mm-dd',dtdt1.Date)+#39' AND '#39+FormatDateTime('yyyy-mm-dd',dtdt2.Date)+#39+'');
 ExecSQL;
end;
  with tgrd1 do
  begin
    DataSource:=DM.Q1;
  Columns['id'].Visible:=False;
  Columns['nomor'].Width.Automatic:= False;
  Columns['nomor'].Width.Value:=30;
  Columns['nomor'].Header.Text:='No.';
//  Columns['nomor'].DataFormat.DateTime:='dd/mm/yyyy';
  Columns['tgl'].Width.Automatic:= False;
  Columns['tgl'].Width.Value:=80;
  Columns['tgl'].DataFormat.DateTime:='dd/mm/yyyy';
  Columns['tgl'].DataFormat.Date:='dd/mm/yyyy';
//  Columns['petak'].Width.Automatic:= False;
//  Columns['petak'].Width.Value:=45;
//  Columns['kategori'].Width.Automatic:= False;
//  Columns['kategori'].Width.Value:=55;
  Columns['status'].Width.Automatic:= False;
  Columns['status'].Width.Value:=55;
  Columns['varietas'].Width.Automatic:= False;
  Columns['varietas'].Width.Value:=60;
//  Columns['areal'].Width.Automatic:= False;
//  Columns['areal'].Width.Value:=55;
//  Columns['areal'].DataFormat.Float:='0.##';
  Columns['masa_tanam'].Width.Automatic:= False;
  Columns['masa_tanam'].Width.Value:=55;
//   Columns['masa_tanam'].Header.TextAlign
  Columns['masa_tanam'].Header.Text:='MT';

  Columns['ohk'].Width.Automatic:= False;
  Columns['ohk'].Width.Value:=35;
  Columns['ohk'].Header.Text:='OHK';
//  Columns['ohk'].TextAlign.Horizontal:=r
  Columns['nama_kebun'].Width.Automatic:= False;
  Columns['nama_kebun'].Width.Value:=80;
  Columns['nama_kebun'].Header.Text:='Kebun';
  Columns['nama_afdeling'].Width.Automatic:= False;
  Columns['nama_afdeling'].Width.Value:=80;
  Columns['nama_afdeling'].Header.Text:='Afdeling';
  Columns['nama_mandor'].Width.Automatic:= False;
  Columns['nama_mandor'].Width.Value:=80;
  Columns['nama_mandor'].Header.Text:='Mandor';
  Columns['kd_blok'].Width.Automatic:= False;
  Columns['kd_blok'].Width.Value:=150;
  Columns['kd_blok'].Header.Text:='Blok';
//  Columns['nama_astan'].Width.Automatic:= False;
//  Columns['nama_astan'].Width.Value:=80;
//  Columns['nama_astan'].Header.Text:='Astan';
  Columns['nama_pekerjaan'].Width.Automatic:= False;
  Columns['nama_pekerjaan'].Width.Value:=220;
  Columns['nama_pekerjaan'].Header.Text:='Pekerjaan';
  end;
  with tgrd2 do
  begin
    Columns['id'].Width.Automatic:= False;
    Columns['id'].Width.Value:=40;
    Columns['id'].Header.Text:='Kode';
    Columns['nama_pekerjaan'].Width.Automatic:= False;
    Columns['nama_pekerjaan'].Width.Value:=200;
    Columns['nama_pekerjaan'].Header.Text:='Pekerjaan';
  end;
  with tgrd4 do
  begin
    Columns['id'].Width.Automatic:= False;
    Columns['id'].Width.Value:=40;
    Columns['id'].Header.Text:='Kode';
    Columns['nama_pekerjaan'].Width.Automatic:= False;
    Columns['nama_pekerjaan'].Width.Value:=200;
    Columns['nama_pekerjaan'].Header.Text:='Pekerjaan';
  end;
  with tgrd5 do
  begin
    Columns['id'].Width.Automatic:= False;
    Columns['id'].Width.Value:=40;
    Columns['id'].Header.Text:='Kode';
    Columns['nama_pekerjaan'].Width.Automatic:= False;
    Columns['nama_pekerjaan'].Width.Value:=200;
    Columns['nama_pekerjaan'].Header.Text:='Pekerjaan';
  end;
end;

procedure TfrmPekerjaan.FormShow(Sender: TObject);
begin
 dt_tgl.SetFocus;
end;

end.
