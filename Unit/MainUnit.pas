unit MainUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Layouts, System.ImageList, FMX.ImgList,
  FMX.MultiView, FMX.TabControl;

type
  TForm1 = class(TForm)
    sty1: TStyleBook;
    il1: TImageList;
    pnl2: TPanel;
    mltvw1: TMultiView;
    btn2: TCornerButton;
    btn3: TCornerButton;
    btn4: TCornerButton;
    btn5: TCornerButton;
    btn6: TCornerButton;
    btn1: TCornerButton;
    tbc1: TTabControl;
    tbtm1: TTabItem;
    stat1: TStatusBar;
    procedure btn6Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  uDM, uPekerjaan;


{$R *.fmx}
{$R *.Windows.fmx MSWINDOWS}

procedure TForm1.btn3Click(Sender: TObject);
begin
 FrmPekerjaan.Show;
end;

procedure TForm1.btn6Click(Sender: TObject);
begin
 Application.Terminate;
end;

end.
